#include "summoner.h"

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);
HWND foundWindow = nullptr;
char foundWindowName[256];
char *_needle;

bool contains(char MyChar[], char Search[]) {
  int LoopOne = 0;
  int LoopTwo;
  int LoopThree;
  int MyCharSize = strlen(MyChar);
  int SearchSize = strlen(Search);
  int Yes = 0;

  while (LoopOne < MyCharSize) {
    if (MyChar[LoopOne] == Search[0]) {
      LoopTwo = 0;
      LoopThree = LoopOne;
      while (LoopTwo < SearchSize) {
        if (MyChar[LoopThree] == Search[LoopTwo])
          Yes++;

        LoopTwo++;
        LoopThree++;
      }
      if (Yes == SearchSize)
        return true;
    }
    LoopOne++;
  }
  return false;
}

HWND FindWindowContatining(char *needle, char **longName) {
  foundWindow = nullptr;
  _needle = needle;

  EnumWindows(EnumWindowsProc, NULL);

  for (; foundWindow == nullptr; Sleep(10));
  *longName = foundWindowName;
  return foundWindow;
}

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam) {
  char class_name[256];
  char title[256];

  GetClassName(hwnd, class_name, sizeof(class_name));
  GetWindowText(hwnd, title, sizeof(title));

  if (contains(title, _needle)) {
    memcpy(foundWindowName, title, 256);
    foundWindow = hwnd;
    return FALSE;
  }

  return TRUE;
}